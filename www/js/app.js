.config(['$ionicAppProvider', function($ionicAppProvider) {
  // Identify app
  $ionicAppProvider.identify({
    // The App ID (from apps.ionic.io) for the server
    app_id: 'b6b291ee',
    // The public API key all services will use for this app
    api_key: '50cc7b37f1613ca8f2f0675b6388bc7c4a64eb2bd4acb223',
    // The GCM project ID (project number) from your Google Developer Console (un-comment if used)
    gcm_id: '617174658949'
  });
}])