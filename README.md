# README #

The project was assigned to me by CXC Programme conducted by Intel and IISc. The project was on IoT.

### What is this repository for? ###

This repo is for developers who want to use cordova for development of web apps. The app has been tested with Intel Galileo Board and on Android Phone. The app should work perfectly on other devices as well.

### How do I get set up? ###

IoT Requirements
* Arduino Compatible Board, LEDs, LM35, Proximity Sensors, Jumper Wires

* Setup
Clone the Repo and follow the instructions on [Cordova Command Line Interface](https://cordova.apache.org/docs/en/4.0.0/guide_cli_index.md.html) to install app on your device

* Board Configuration
Check the instructions on the .ino